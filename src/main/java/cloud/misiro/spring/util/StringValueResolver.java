package cloud.misiro.spring.util;

public interface StringValueResolver {
    String resolveStringValue(String strVal);
}
