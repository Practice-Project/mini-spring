package cloud.misiro.spring.context;

public interface ApplicationEventPublisher {
    void publishEvent(ApplicationEvent event);
}
