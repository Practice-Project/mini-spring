package cloud.misiro.spring.context;

import cloud.misiro.spring.beans.factory.HierarchicalBeanFactory;
import cloud.misiro.spring.beans.factory.ListableBeanFactory;
import cloud.misiro.spring.core.io.ResourceLoader;

public interface ApplicationContext extends ListableBeanFactory, HierarchicalBeanFactory, ResourceLoader, ApplicationEventPublisher {
}
