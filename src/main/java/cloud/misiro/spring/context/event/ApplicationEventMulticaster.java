package cloud.misiro.spring.context.event;

import cloud.misiro.spring.context.ApplicationEvent;
import cloud.misiro.spring.context.ApplicationListener;

public interface ApplicationEventMulticaster {
    void addApplicationListener(ApplicationListener<?> listener);

    void removeApplicationListener(ApplicationListener<?> listener);

    void multicastEvent(ApplicationEvent event);
}
