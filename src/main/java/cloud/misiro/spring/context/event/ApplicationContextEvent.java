package cloud.misiro.spring.context.event;

import cloud.misiro.spring.context.ApplicationContext;
import cloud.misiro.spring.context.ApplicationEvent;

public abstract class ApplicationContextEvent extends ApplicationEvent {
    public ApplicationContextEvent(ApplicationContext source) {
        super(source);
    }

    public final ApplicationContext getApplicationContext() {
        return (ApplicationContext) getSource();
    }
}
