package cloud.misiro.spring.context.event;

import cloud.misiro.spring.context.ApplicationContext;

public class ContextRefreshedEvent extends ApplicationContextEvent{
    public ContextRefreshedEvent(ApplicationContext source) {
        super(source);
    }
}
