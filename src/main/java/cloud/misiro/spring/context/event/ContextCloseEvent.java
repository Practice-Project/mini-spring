package cloud.misiro.spring.context.event;

import cloud.misiro.spring.context.ApplicationContext;

public class ContextCloseEvent extends ApplicationContextEvent{
    public ContextCloseEvent(ApplicationContext source) {
        super(source);
    }
}
