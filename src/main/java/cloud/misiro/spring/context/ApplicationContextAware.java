package cloud.misiro.spring.context;

import cloud.misiro.spring.beans.BeansException;
import cloud.misiro.spring.beans.factory.Aware;

public interface ApplicationContextAware extends Aware {
    void setApplicationContext(ApplicationContext applicationContext) throws BeansException;
}
