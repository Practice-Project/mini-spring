package cloud.misiro.spring.context;

import cloud.misiro.spring.beans.BeansException;

public interface ConfigurableApplicationContext extends ApplicationContext {
    void refresh() throws BeansException;

    void close();

    void registerShutdownHook();
}
