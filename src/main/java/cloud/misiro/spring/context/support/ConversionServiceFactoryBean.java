package cloud.misiro.spring.context.support;

import cloud.misiro.spring.beans.factory.FactoryBean;
import cloud.misiro.spring.beans.factory.InitializingBean;
import cloud.misiro.spring.core.convert.ConversionService;
import cloud.misiro.spring.core.convert.converter.Converter;
import cloud.misiro.spring.core.convert.converter.ConverterFactory;
import cloud.misiro.spring.core.convert.converter.ConverterRegistry;
import cloud.misiro.spring.core.convert.converter.GenericConverter;
import cloud.misiro.spring.core.convert.support.DefaultConversionService;
import cloud.misiro.spring.core.convert.support.GenericConversionService;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.TypeUtil;

import java.util.Set;

public class ConversionServiceFactoryBean implements FactoryBean<ConversionService>, InitializingBean {

    private Set<?> converters;

    private GenericConversionService conversionService;

    @Override
    public ConversionService getObject() throws Exception {
        return conversionService;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        conversionService = new DefaultConversionService();
        registerConverters(converters, conversionService);
    }

    public void setConverters(Set<?> converters) {
        this.converters = converters;
    }

    private void registerConverters(Set<?> converters, ConverterRegistry registry) {
        if (converters != null) {
            for (Object converter : converters) {
                if (converter instanceof GenericConverter) {
                    registry.addConverter((GenericConverter) converter);
                } else if (converter instanceof Converter<?, ?>) {
                    registry.addConverter((Converter<?, ?>) converter);
                } else if (converter instanceof ConverterFactory<?, ?>) {
                    registry.addConverterFactory((ConverterFactory<?, ?>) converter);
                } else {
                    throw new IllegalArgumentException("Each converter object must implement one of the " +
                            "Converter, ConverterFactory, or GenericConverter interfaces");
                }
            }
        }
    }
}
