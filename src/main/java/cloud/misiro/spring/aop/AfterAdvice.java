package cloud.misiro.spring.aop;

import org.aopalliance.aop.Advice;

public interface AfterAdvice extends Advice {
}
