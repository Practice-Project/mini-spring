package cloud.misiro.spring.aop.framework;

public interface AopProxy {
    Object getProxy();
}
