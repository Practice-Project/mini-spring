package cloud.misiro.spring.aop;

public interface ClassFilter {
    boolean matches(Class<?> clazz);
}
