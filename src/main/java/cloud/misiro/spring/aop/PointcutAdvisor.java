package cloud.misiro.spring.aop;

public interface PointcutAdvisor extends Advisor {
    Pointcut getPointCut();
}
