package cloud.misiro.spring.beans.factory.config;

import cloud.misiro.spring.beans.factory.HierarchicalBeanFactory;
import cloud.misiro.spring.core.convert.ConversionService;
import cloud.misiro.spring.util.StringValueResolver;

public interface ConfigurableBeanFactory extends HierarchicalBeanFactory, SingletonBeanRegistry {
    void addBeanPostProcessor(BeanPostProcessor beanPostProcessor);

    void destroySingletons();

    void addEmbeddedValueResolver(StringValueResolver valueResolver);

    String resolveEmbeddedValue(String value);

    void setConversionService(ConversionService conversionService);

    ConversionService getConversionService();
}
