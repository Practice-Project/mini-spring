package cloud.misiro.spring.beans.factory;

import cloud.misiro.spring.beans.BeansException;
import cloud.misiro.spring.beans.factory.config.AutowireCapableBeanFactory;
import cloud.misiro.spring.beans.factory.config.BeanDefinition;
import cloud.misiro.spring.beans.factory.config.BeanPostProcessor;
import cloud.misiro.spring.beans.factory.config.ConfigurableBeanFactory;

public interface ConfigurableListableBeanFactory extends ListableBeanFactory, AutowireCapableBeanFactory, ConfigurableBeanFactory {

    BeanDefinition getBeanDefinition(String beanName) throws BeansException;

    void preInstantiateSingletons() throws BeansException;

    void addBeanPostProcessor(BeanPostProcessor beanPostProcessor);
}
