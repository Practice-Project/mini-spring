package cloud.misiro.spring.beans.factory.support;

import cloud.misiro.spring.beans.BeansException;
import cloud.misiro.spring.core.io.Resource;
import cloud.misiro.spring.core.io.ResourceLoader;

public interface BeanDefinitionReader {

    BeanDefinitionRegistry getRegistry();

    ResourceLoader getResourceLoader();

    void loadBeanDefinitions(Resource resource) throws BeansException;

    void loadBeanDefinitions(String location) throws BeansException;

    void loadBeanDefinitions(String[] locations) throws BeansException;
}
