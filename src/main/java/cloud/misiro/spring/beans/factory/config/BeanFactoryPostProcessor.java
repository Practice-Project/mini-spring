package cloud.misiro.spring.beans.factory.config;

import cloud.misiro.spring.beans.BeansException;
import cloud.misiro.spring.beans.factory.ConfigurableListableBeanFactory;

public interface BeanFactoryPostProcessor {

    void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException;
}
