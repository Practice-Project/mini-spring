package cloud.misiro.spring.beans.factory;

import cloud.misiro.spring.beans.BeansException;

public interface ObjectFactory<T> {
    T getObject() throws BeansException;
}
