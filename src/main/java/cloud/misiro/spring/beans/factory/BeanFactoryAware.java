package cloud.misiro.spring.beans.factory;

import cloud.misiro.spring.beans.BeansException;

public interface BeanFactoryAware extends Aware {
    void setBeanFactory(BeanFactory beanFactory) throws BeansException;
}
