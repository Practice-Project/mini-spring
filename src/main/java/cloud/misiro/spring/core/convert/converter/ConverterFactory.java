package cloud.misiro.spring.core.convert.converter;

import cloud.misiro.spring.core.convert.converter.Converter;

public interface ConverterFactory<S, R> {
    <T extends R> Converter<S, T> getConverter(Class<T> targetType);
}
