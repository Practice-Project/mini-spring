package cloud.misiro.spring.core.convert.converter;

import cloud.misiro.spring.core.convert.converter.Converter;
import cloud.misiro.spring.core.convert.converter.ConverterFactory;

public interface ConverterRegistry {
    void addConverter(Converter<?, ?> converter);

    void addConverterFactory(ConverterFactory<?, ?> converterFactory);

    void addConverter(GenericConverter converter);
}
