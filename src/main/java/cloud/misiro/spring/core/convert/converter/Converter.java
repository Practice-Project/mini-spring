package cloud.misiro.spring.core.convert.converter;

public interface Converter<S, T> {
    T convert(S source);
}
