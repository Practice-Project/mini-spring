package cloud.misiro.spring.core.io;

public interface ResourceLoader {
    Resource getResource(String location);
}
