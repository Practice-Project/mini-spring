package cloud.misiro.test.common.event;

import cloud.misiro.spring.context.ApplicationListener;
import cloud.misiro.spring.context.event.ContextCloseEvent;

public class ContextClosedEventListener implements ApplicationListener<ContextCloseEvent> {
    @Override
    public void onApplicationEvent(ContextCloseEvent event) {
        System.out.println(this.getClass().getName());
    }
}
