package cloud.misiro.test.common.event;

import cloud.misiro.spring.context.ApplicationContext;
import cloud.misiro.spring.context.event.ApplicationContextEvent;

public class CustomEvent extends ApplicationContextEvent {
    public CustomEvent(ApplicationContext source) {
        super(source);
    }
}
