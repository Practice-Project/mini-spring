package cloud.misiro.test.common;

import cloud.misiro.spring.beans.BeansException;
import cloud.misiro.spring.beans.factory.ConfigurableListableBeanFactory;
import cloud.misiro.spring.beans.factory.PropertyValue;
import cloud.misiro.spring.beans.factory.PropertyValues;
import cloud.misiro.spring.beans.factory.config.BeanDefinition;
import cloud.misiro.spring.beans.factory.config.BeanFactoryPostProcessor;

public class CustomBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        BeanDefinition personBeanDefinition = beanFactory.getBeanDefinition("person");
        PropertyValues propertyValues = personBeanDefinition.getPropertyValues();
        propertyValues.addPropertyValue(new PropertyValue("name", "ivy"));
    }
}
