package cloud.misiro.test.common;

import cloud.misiro.spring.aop.ThrowsAdvice;

import java.lang.reflect.Method;
import java.util.Arrays;

public class WorldServiceAfterThrowingAdvice implements ThrowsAdvice {

    public void afterThrowing(IllegalArgumentException ex) {
        System.out.println("Do something after throwing ex");
    }

    public void afterThrowing(Method method, Object[] args, Object target, IllegalStateException ex) {
        System.out.println(method.getName());
        System.out.println(Arrays.toString(args));
        System.out.println(target.getClass());
        System.out.println("Do something after throwing ex");
    }
}
