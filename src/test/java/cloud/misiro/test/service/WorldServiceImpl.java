package cloud.misiro.test.service;

public class WorldServiceImpl implements WorldService {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void explode() {
        System.out.println("The Earth is going to explode");
    }

    public void explodeFailure() {
        System.out.println("The earth explode failed");
        throw new IllegalStateException("failed");
    }

    @Override
    public String toString() {
        return "WorldServiceImpl{" +
                "name='" + name + '\'' +
                '}';
    }
}
