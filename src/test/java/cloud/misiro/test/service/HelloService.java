package cloud.misiro.test.service;

import cloud.misiro.spring.beans.BeansException;
import cloud.misiro.spring.beans.factory.BeanFactory;
import cloud.misiro.spring.beans.factory.BeanFactoryAware;
import cloud.misiro.spring.context.ApplicationContext;
import cloud.misiro.spring.context.ApplicationContextAware;

public class HelloService implements ApplicationContextAware, BeanFactoryAware {

    private ApplicationContext applicationContext;

    private BeanFactory beanFactory;

    public String sayHello() {
        System.out.println("hello");
        return "hello";
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }
}
