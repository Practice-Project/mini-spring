package cloud.misiro.test.service;

public interface WorldService {
    void explode();

    void explodeFailure();
}
