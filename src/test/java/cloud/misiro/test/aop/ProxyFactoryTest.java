package cloud.misiro.test.aop;

import cloud.misiro.spring.aop.TargetSource;
import cloud.misiro.spring.aop.aspectj.AspectJExpressionPointcutAdvisor;
import cloud.misiro.spring.aop.framework.ProxyFactory;
import cloud.misiro.spring.aop.framework.adapter.AfterReturningAdviceInterceptor;
import cloud.misiro.spring.aop.framework.adapter.MethodBeforeAdviceInterceptor;
import cloud.misiro.test.common.WorldServiceAfterReturningAdvice;
import cloud.misiro.test.common.WorldServiceBeforeAdvice;
import cloud.misiro.test.service.WorldService;
import cloud.misiro.test.service.WorldServiceImpl;
import org.junit.Test;

public class ProxyFactoryTest {
    @Test
    public void testAdvisor() throws Exception {
        WorldService worldService = new WorldServiceImpl();

        String expression = "execution(* cloud.misiro.test.service.WorldService.explode(..))";
        AspectJExpressionPointcutAdvisor advisor = new AspectJExpressionPointcutAdvisor();
        advisor.setExpression(expression);
        MethodBeforeAdviceInterceptor methodInterceptor = new MethodBeforeAdviceInterceptor(new WorldServiceBeforeAdvice());
        advisor.setAdvice(methodInterceptor);
        AspectJExpressionPointcutAdvisor advisor1 = new AspectJExpressionPointcutAdvisor();
        advisor1.setExpression(expression);
        AfterReturningAdviceInterceptor afterReturningAdviceInterceptor = new AfterReturningAdviceInterceptor(new WorldServiceAfterReturningAdvice());
        advisor1.setAdvice(afterReturningAdviceInterceptor);
        ProxyFactory factory = new ProxyFactory();
        TargetSource targetSource = new TargetSource(worldService);
        factory.setTargetSource(targetSource);
        factory.setProxyTargetClass(true);
        factory.addAdvisor(advisor);
        factory.addAdvisor(advisor1);
        WorldService proxy = (WorldService) factory.getProxy();
        proxy.explode();
    }
}
