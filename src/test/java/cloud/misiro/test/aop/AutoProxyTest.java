package cloud.misiro.test.aop;

import cloud.misiro.spring.context.support.ClassPathXmlApplicationContext;
import cloud.misiro.test.service.WorldService;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AutoProxyTest {

    @Test
    public void testAutoProxy() throws Exception {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:auto-proxy.xml");

        WorldService worldService = applicationContext.getBean("worldService", WorldService.class);
        worldService.explode();

        WorldService worldService1 = applicationContext.getBean("worldService", WorldService.class);
        assertThat(worldService == worldService1).isTrue();
        System.out.println(worldService1);
    }
}
