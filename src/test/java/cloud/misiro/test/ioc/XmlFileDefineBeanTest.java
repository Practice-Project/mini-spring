package cloud.misiro.test.ioc;

import cloud.misiro.spring.beans.factory.support.DefaultListableBeanFactory;
import cloud.misiro.spring.beans.factory.xml.XmlBeanDefinitionReader;
import cloud.misiro.test.bean.Car;
import cloud.misiro.test.bean.Person;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class XmlFileDefineBeanTest {

    @Test
    public void testXmlFile() throws Exception {
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(beanFactory);
        beanDefinitionReader.loadBeanDefinitions("classpath:spring.xml");

        Person person = (Person) beanFactory.getBean("person");
        System.out.println(person);
        assertThat(person.getName()).isEqualTo("derek");
        assertThat(person.getCar().getBrand()).isEqualTo("porsche");

        Car car = (Car) beanFactory.getBean("car");
        System.out.println(car);
        assertThat(car.getBrand()).isEqualTo("porsche");
    }
}
