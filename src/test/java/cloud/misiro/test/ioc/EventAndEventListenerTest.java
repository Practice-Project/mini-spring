package cloud.misiro.test.ioc;

import cloud.misiro.spring.context.support.ClassPathXmlApplicationContext;
import cloud.misiro.test.common.event.CustomEvent;
import org.junit.Test;

public class EventAndEventListenerTest {

    @Test
    public void testEventListener() throws Exception {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:event-and-event-listener.xml");
        applicationContext.publishEvent(new CustomEvent(applicationContext));

        applicationContext.registerShutdownHook();
    }
}
