package cloud.misiro.test.ioc;

import cloud.misiro.spring.context.support.ClassPathXmlApplicationContext;
import cloud.misiro.test.bean.A;
import cloud.misiro.test.bean.B;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CircularReferenceWithProxyBeanTest {

    @Test
    public void testCircularReference() throws Exception {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:circular-reference-with-proxy-bean.xml");
        A a = applicationContext.getBean("a", A.class);
        B b = applicationContext.getBean("b", B.class);

        assertThat(b.getA() == a).isTrue();
    }
}
